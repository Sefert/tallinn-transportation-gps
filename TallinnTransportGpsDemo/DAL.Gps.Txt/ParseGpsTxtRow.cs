﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Domain;
using Domain.Enums;

namespace DAL
{
    public class ParseGpsTxtRow
    {
        public TransportationService Parse(string row)
        {
            string[] info = AllowedCharacters(row) && HasCorrectFormat(row)? row.Split(',') : null;

            if (info != null && !EnteredEmptyValues(info))
            {
                if (OneDigit(info[0]) && EightDigit(info[2], info[3]) && DegreesZeroToFullCircle(info[5]))
                {
                    return new TransportationService()
                    {
                        Id = ToNumeric(info[6]),
                        Type = (TransportationType) ToNumeric(info[0]),
                        Number = info[1],
                        Angle = Convert.ToInt16(ToNumeric(info[5])),
                        Coordinate = new GlobalPositioningSystemCoordinate()
                        {
                            Latitude = ToNumeric(info[3]) / 1_000_000d,
                            Longitude = ToNumeric(info[2]) / 1_000_000d,
                            Elevation = ToNumeric(info[4]) / 1_000_000d
                        }
                    };
                }
            }

            return null;
        }

        private Int32 ToNumeric(string value)
        {
            return Int32.TryParse(value, out int number) ? number :  0;
        }

        private bool OneDigit(params string[] value)
        {
            return  value.All(val => Regex.IsMatch(val, @"^[1-3]{1}$"));
        }
        private bool EightDigit(params string[] value)
        {
            return  value.All(val => Regex.IsMatch(val, @"^-?[0-9]{8}$"));
        }
        private bool DegreesZeroToFullCircle(params string[] value)
        {
            return  value.All(val => Regex.IsMatch(val,
                @"^[0]{1}$|^[1-9][0-9]{1}$|^[1-2][0-9]{2}$|^[3][0-5][0-9]$"));
        }
        private bool AllowedCharacters(params string[] value)
        {
            return  !value.All(val => Regex.IsMatch(val, @"[^A-Za-z0-9,]+"));
        }
        
        private bool HasCorrectFormat(string value)
        {
            int commaCounter = Regex.Matches(value, @",").Count;
            if (commaCounter == 6)
            {
                return true;
            }
            return false;
        }

        private bool EnteredEmptyValues(params string[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (i != 4)
                {
                    if (values[i] == null)
                    {
                        return true;
                    }
                    if (values[i].Length< 1 || values[i] == "" )
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}