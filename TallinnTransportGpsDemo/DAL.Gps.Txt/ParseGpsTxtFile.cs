﻿using System.Collections.Generic;
using Domain;

namespace DAL
{
    public class ParseGpsTxtFile : ParseGpsTxtRow
    {
        public readonly List<TransportationService> TransportationServiceList;
        public readonly List<string> SkippedRow = new List<string>();
        public ParseGpsTxtFile(List<string> rowList)
        {
            TransportationServiceList = new List<TransportationService>();
            foreach (var row in rowList)
            {
                TransportationService transport = Parse(row);
                if(transport != null)
                {
                    TransportationServiceList.Add(transport);
                }
                else
                {
                    SkippedRow.Add(row);
                }
            }
        }
    }
}