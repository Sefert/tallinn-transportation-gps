﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Domain;


namespace DAL
{
    public class ReadFile
    {
        public readonly List<string> FileRowList;
        public ReadFile(string path)
        {
            using (WebClient client = new WebClient()) {
                Stream stream = client.OpenRead(path);
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string row;
                        FileRowList = new List<string>();
                        while ((row = reader.ReadLine()) != null)
                        {
                            FileRowList.Add(row);
                        }
                    }
            }
        }
    }
}