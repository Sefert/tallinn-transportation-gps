﻿
using Domain;
using Domain.Settings;
using Microsoft.Extensions.Options;

namespace DAL
{
    public class DataContext
    {
        public readonly TransportationPark TransportationPark;

        public DataContext(IOptions<GpsTxtSettings> appSettings)
        {
            var file = new ReadFile(appSettings.Value.GpsPath).FileRowList;
            TransportationPark = new TransportationPark {
                TransportationList = new ParseGpsTxtFile(file).TransportationServiceList
            };
        }
    }
}