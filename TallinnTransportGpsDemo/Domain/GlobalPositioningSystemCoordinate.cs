﻿using System;

namespace Domain
{
    public class GlobalPositioningSystemCoordinate
    {
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }
        public Double Elevation { get; set; }
    }
}