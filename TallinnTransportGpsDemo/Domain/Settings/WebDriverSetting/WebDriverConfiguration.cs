﻿namespace Domain.Settings.WebDriverSetting
{
    public class WebDriverConfiguration
    {
        public Firefox Firefox { get; set; }
        public Chrome Chrome { get; set; }
        public Edge Edge { get; set; }
    }
}