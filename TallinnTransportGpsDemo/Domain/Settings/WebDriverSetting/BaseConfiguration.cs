﻿using System;

namespace Domain.Settings.WebDriverSetting
{
    public class BaseSetting
    {
        public String Key { get; set; }
        public String Location { get; set; }
    }
}