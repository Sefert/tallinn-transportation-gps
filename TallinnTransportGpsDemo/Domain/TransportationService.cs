﻿using System;
using Domain.Enums;

namespace Domain
{
    public class TransportationService
    {
        public int Id { get; set; }
        public TransportationType Type { get; set; }
        public string Number { get; set; }
        public Int16 Angle { get; set; }
        public GlobalPositioningSystemCoordinate Coordinate { get; set; }
    }
}