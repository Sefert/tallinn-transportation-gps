﻿using System;
using System.Collections.Generic;
using DAL;
using Domain;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.ViewModels;
using Controller = Microsoft.AspNetCore.Mvc.Controller;

namespace WebApp.Controllers
{
    public class TallinnController : Controller
    {
        private readonly ILogger<TallinnController> _logger;
        private readonly DataContext _context;

        public TallinnController(ILogger<TallinnController> logger, DataContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        
        //[HttpGet("{id}")] // Matches '/Home/Hello/{id}'
        public IActionResult Bus(string id)
        {
            var vm = new TransportationLocationViewModel();
            if (!String.IsNullOrEmpty(id))
            {
                vm = new TransportationLocationViewModel()
                {
                    TransportationList = new List<TransportationService>(_context.TransportationPark.TransportationList.FindAll(tr => tr.Number == id && tr.Type == TransportationType.Bus)),
                    ServiceNumber = id
                };
            }
            return View(vm);
        }

        public IActionResult Tram(string id)
        {
            var vm = new TransportationLocationViewModel();
            if (!String.IsNullOrEmpty(id))
            {
                vm = new TransportationLocationViewModel()
                {
                    TransportationList = new List<TransportationService>(_context.TransportationPark.TransportationList.FindAll(tr => tr.Number == id && tr.Type == TransportationType.Tram)),
                    ServiceNumber = id
                };
            }
            return View(vm);
        }
        
        public IActionResult TrolleyBus(string id)
        {
            var vm = new TransportationLocationViewModel();
            if (!String.IsNullOrEmpty(id))
            {
                vm = new TransportationLocationViewModel()
                {
                    TransportationList = new List<TransportationService>(_context.TransportationPark.TransportationList.FindAll(tr => tr.Number == id && tr.Type == TransportationType.TrolleyBus)),
                    ServiceNumber = id
                };
            }
            return View(vm);
        }
    }
}