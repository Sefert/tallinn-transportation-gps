﻿using System.Collections;
using System.Collections.Generic;
using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.ViewModels
{
    //TODO: Show embed map on page
    public class TransportationLocationViewModel
    {
        public TransportationService TransportationService { get; set; }
        public List<TransportationService> TransportationList { get; set; } 
        public string ServiceNumber { get; set; }
        public string? ShowNumber { get; set; }
    }
}