﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using TallinnTransportGpsTest.Enum;

namespace TallinnTransportGpsTest.WebDriverFactory
{
    public class WebDriverSetup : WebDriverFactorySetup
    {
        private readonly Browser _type;
        protected IWebDriver Driver;
        protected WebDriverSetup(Browser type) => _type = type;

        [OneTimeSetUp]
        protected override void OneTimeSetUp()
        {
            base.OneTimeSetUp();
            
            Driver = WebDriverFactory.GetBrowserWebDriver(_type);
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
            Driver.Manage().Window.Maximize();}

        [TearDown]
        protected virtual void TearDown()
        {
            Driver.Quit();
        }
        
    }
}