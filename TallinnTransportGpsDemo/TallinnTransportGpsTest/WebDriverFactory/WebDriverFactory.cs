﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using TallinnTransportGpsTest.Enum;

namespace TallinnTransportGpsTest.WebDriverFactory
{
    public class WebDriverFactory
    {
        public IWebDriver GetBrowserWebDriver(Browser browser)
        {
            switch(browser)
            {
                case Browser.Edge:
                    return new EdgeDriver();
                case Browser.Firefox:
                    return new FirefoxDriver(GetFirefoxOptions());
                case Browser.Chrome:
                    return new ChromeDriver(ChromeDriverService.CreateDefaultService(), GetChromeOptions());
                default :
                    return GetBrowserWebDriver(Browser.Chrome);
            }
        }

        private ChromeOptions GetChromeOptions()
        {
            var chromeOptions = new ChromeOptions();
            
            var downloadDirectory = "../../../Downloads";

            chromeOptions.AddUserProfilePreference("download.default_directory", downloadDirectory);
            chromeOptions.AddUserProfilePreference("download.prompt_for_download", false);
            chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");
            chromeOptions.AddUserProfilePreference("block_third_party_cookies", true);

            return chromeOptions;
        }
        
        private FirefoxOptions GetFirefoxOptions()
        {
            var firefoxOptions = new FirefoxOptions();
            
            firefoxOptions.AcceptInsecureCertificates = true;
            
            return firefoxOptions;
        }
    }
}