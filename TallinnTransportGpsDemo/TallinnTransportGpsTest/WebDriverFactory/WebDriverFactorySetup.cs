﻿using System;
using System.IO;
using Domain.Settings.WebDriverSetting;
using NUnit.Framework;

namespace TallinnTransportGpsTest.WebDriverFactory
{
    public class WebDriverFactorySetup : TestLog
    {
        /*Use for loading information from testsettings.json*/
        protected WebDriverConfiguration WebDriverConfiguration;
        protected WebDriverFactory WebDriverFactory;// = new WebDriverFactory();

        [OneTimeSetUp]
        protected override void OneTimeSetUp()
        {
            base.OneTimeSetUp();
            //"C:\\Projects\\TransportGps\\TallinnTransportGpsDemo\\TallinnTransportGpsTest\\"
            WebDriverConfiguration = TestConfiguration.GetTestConfiguration(Path.Combine(Environment.CurrentDirectory, @"..\\..\\..\\..\\"));
            WebDriverFactory = new WebDriverFactory();
        }
    }
}