﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using log4net;
using log4net.Config;
using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace TallinnTransportGpsTest
{
    public class TestLog
    {
        
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        private string _testName, _testResult, _message;
        private readonly List<string> _events  = new List<string>();

        protected void LogInfo(string text)
        {
            //string status;
            if (_message != null)
            {
                var status = TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Inconclusive ?
                    "Passed" : TestContext.CurrentContext.Result.Outcome.Status.ToString();

                _events.Add(_message + ": " + status); 
            }
            _message = text;
        }
        
        [OneTimeSetUp]
        protected virtual void OneTimeSetUp()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            //string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            XmlConfigurator.ConfigureAndWatch(logRepository, new FileInfo("../../../log4net.config"));

            GlobalContext.Properties["LogFilePath"] = "../../../Log";
            log4net.Util.SystemInfo.NullText = "";
        }
        
        
        [SetUp]
        protected virtual void SetUp()
        {
            _testName = "Test Name : " + TestContext.CurrentContext.Test.Name;
        }
        
        [TearDown]
        protected virtual void AfterTest()
        {
            bool testPassed = TestContext.CurrentContext.Result.Outcome.Status.Equals(TestStatus.Passed);
            
            _testResult = testPassed ? "TEST PASSED" : "TEST FAILED";
            
            LogInfo("");
            
            StringBuilder message = new StringBuilder();
            message.Append(_testName + " -> ");
            foreach (var elem in _events)
            {
                message.Append(elem + " -> ");
            }
            message.Append(_testResult);

            if (testPassed)
            { 
                Log.Info(message.ToString());
            }
            else
            { 
                Log.Debug(message.ToString()); 
            }
            
            _message = null;
            _events.Clear();
        }
    }
}