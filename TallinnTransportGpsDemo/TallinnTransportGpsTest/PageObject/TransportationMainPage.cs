﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;


namespace TallinnTransportGpsTest.PageObject
{
    public class TransportationMainPage
    {
        private readonly IWebDriver _driver;

        public TransportationMainPage(IWebDriver driver) => _driver = driver;

        public IWebElement BusLink => _driver.FindElement(By.CssSelector("ul.right.hide-on-med-and-down>li:nth-child(1)>a"));
        public IWebElement TramLink => _driver.FindElement(By.CssSelector("ul.right.hide-on-med-and-down>li:nth-child(2)>a"));
        public IWebElement TrolleyBusLink => _driver.FindElement(By.CssSelector("ul.right.hide-on-med-and-down>li:nth-child(3)>a"));
        public IWebElement Privacy => _driver.FindElement(By.CssSelector("div.container>a[href*='/Tallinn/Privacy']"));
        
        /*
         * Obsolete, although can be still used with SeleniumExtras.PageObjects
         */
        /*[FindsBy(How = How.CssSelector, Using = "ul.right.hide-on-med-and-down>li:nth-child(3)>a")]
        public IWebElement TrolleyLink { get; set; }
        
        [FindsBy(How = How.CssSelector, Using = "div.container>a[href*='/Tallinn/Privacy']")]
        public IWebElement Privacy { get; set; }
        
        [FindsBy(How = How.CssSelector, Using = "ul.right.hide-on-med-and-down>li:nth-child(2)>a")]
        public IWebElement TramLink { get; set; }*/
    }
}