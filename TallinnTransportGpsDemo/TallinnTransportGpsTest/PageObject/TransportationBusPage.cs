﻿using OpenQA.Selenium;

namespace TallinnTransportGpsTest.PageObject
{
    public class TransportationBusPage
    {
        private readonly IWebDriver _driver;

        public TransportationBusPage(IWebDriver driver) => _driver = driver;
        
        public IWebElement SearchLabel => _driver.FindElement(By.CssSelector("label[for='transportation_number']"));
        public IWebElement SearchBox => _driver.FindElement(By.Id("transportation_number"));
        public IWebElement SearchButton => _driver.FindElement(By.ClassName("btn-floating"));
    }
}