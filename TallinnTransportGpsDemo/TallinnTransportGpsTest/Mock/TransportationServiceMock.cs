﻿using System;
using Domain;
using Domain.Enums;

namespace TallinnTransportGpsTest.Mock
{
    public class TransportationServiceMock
    {
        public readonly TransportationService ExpectedTransportation;
        public TransportationServiceMock(int id, TransportationType type, string number,
                                            Int16 angle, Double latitude, Double longitude)
        {
            ExpectedTransportation = new TransportationService()
            {
                Id = id,
                Type = type,
                Number = number,
                Angle = angle,
                Coordinate = new GlobalPositioningSystemCoordinate()
                {
                    Latitude = latitude,
                    Longitude = longitude
                }
            };
        }
    }
}