using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using DAL;
using Domain;
using Domain.Enums;
using NUnit.Framework;
using TallinnTransportGpsTest.Mock;

namespace TallinnTransportGpsTest.UnitTest.DalGpsTxtTest
{
    public class ParseGpsTxtRow : BaseUnitTest
    {
        [Test]
        public void ParseReturnsCorrectTransportationServiceByJson()
        {
            LogInfo("Creating expected transportation");
            TransportationService expectedTram = new TransportationServiceMock(96, TransportationType.Tram,
                "1", 110, 59.4434, 24.75017).ExpectedTransportation;
            TransportationService expectedBus = new TransportationServiceMock(96, TransportationType.Bus,
                "1", 110, 59.4434, 24.75017).ExpectedTransportation;    
            TransportationService expectedTrolleyBus = new TransportationServiceMock(96, TransportationType.TrolleyBus,
                "1", 110, 59.4434, 24.75017).ExpectedTransportation;

            LogInfo("Creating transportation from rows");
            TransportationService actualTram = new DAL.ParseGpsTxtRow().Parse("3,1,24750170,59443400,,110,96");
            TransportationService actualBus = new DAL.ParseGpsTxtRow().Parse("2,1,24750170,59443400,,110,96");
            TransportationService actualTrolleyBus = new DAL.ParseGpsTxtRow().Parse("1,1,24750170,59443400,,110,96");
            
            LogInfo("Comparing expected transportation to actual");
            Assert.AreEqual(JsonSerializer.Serialize(expectedTram), JsonSerializer.Serialize(actualTram));
            
            Assert.AreEqual(JsonSerializer.Serialize(expectedBus), JsonSerializer.Serialize(actualBus));
            Assert.AreEqual(JsonSerializer.Serialize(expectedTrolleyBus), JsonSerializer.Serialize(actualTrolleyBus));
            Assert.AreNotEqual(JsonSerializer.Serialize(expectedTram), JsonSerializer.Serialize(actualBus));
        }

        [Test]
        public void ParseSkipsRowTransportationTypeNotOneDigitOneToThree()
        {
            LogInfo("Parsing rows");
            var rowList = new List<string>();
            rowList.Add("22,1,24750170,59443400,,110,96");
            rowList.Add("5,1,24750170,59443400,,110,96");
            rowList.Add("1A,1,24750170,59443400,,110,96");
            rowList.Add("A1,1,24750170,59443400,,110,96");
            rowList.Add(Tram + ",1,24750170,59443400,,110,96");
            rowList.Add("3 3,1,24750170,59443400,,110,96");
            
            ParseGpsTxtFile result = new ParseGpsTxtFile(rowList);
            
            LogInfo("Is only one row");
            Assert.That(result.TransportationServiceList.Count, Is.EqualTo(1));
            LogInfo("Row is Tram");
            Assert.True(result.TransportationServiceList.All(tp => tp.Type == TransportationType.Tram));
        }
        
        [Test]
        public void ParseSkipsRowIfNotCorrectCoordinate()
        {
            var rowList = new List<string>();
            
            rowList.Add(Bus + ",1,24750170,59443400,,110,96");
            rowList.Add(Tram + ",1,247A0170,59F43400,,110,96");
            rowList.Add(Trolleybus + ",1,247501700,259443400,,110,96");

            ParseGpsTxtFile result = new ParseGpsTxtFile(rowList);
            
            Assert.That(result.TransportationServiceList.Count, Is.EqualTo(1));
            Assert.True(result.TransportationServiceList.All(tp => tp.Type == TransportationType.Bus));
        }
        
        [Test]
        public void ParseSkipsRowIfNotCorrectAngle()
        {
            var rowList = new List<string>();
            rowList.Add(Bus + ",1,24750170,59443400,,0,96");
            rowList.Add(Bus + ",1,24750170,59443400,,10,96");
            rowList.Add(Bus + ",1,24750170,59443400,,270,96");
            rowList.Add(Bus + ",1,24750170,59443400,,310,96");
            rowList.Add(Tram + ",1,24750170,59443400,,360,96");
            rowList.Add(Tram + ",1,24750170,59443400,,370,96");
            rowList.Add(Tram + ",1,24750170,59443400,,410,96");
            rowList.Add(Tram + ",1,24750170,59443400,,01,96");

            
            ParseGpsTxtFile result = new ParseGpsTxtFile(rowList);

            Assert.That(result.TransportationServiceList.Count, Is.EqualTo(4));
            Assert.True(result.TransportationServiceList.All(tp => tp.Type == TransportationType.Bus));
        }
        
        [Test]
        public void ParseSkipsRowIfNotAllowedCharacter()
        {
            var rowList = new List<string>();
            rowList.Add(Bus + ",1,24750170,59443400,,10,96");
            rowList.Add(Tram + ",1,24750170,59443400,>,10,96");
            rowList.Add(Tram + ",1,24750170,59443400,;,10,96");
            rowList.Add(Tram + ",1,24750170,59443400,',10,96");
            rowList.Add(Tram + ",1,24750170,59443400,\",10,96");
            rowList.Add(Tram + ",1,24750170,59443400,<,10,96");
            rowList.Add(Tram + ",1,24750170,59443400,\\,10,96");
            rowList.Add(Tram + ",1,24750170,59443400,|,10,96");
            rowList.Add(Tram + ",1,24750170,59443400,&,10,96");
            rowList.Add(Tram + ",1,24750170,59443400,^,01,96");
            rowList.Add(Tram + ",1,24750170,59443400,$,01,96");
            rowList.Add(Tram + ",1,24750170,59443400,#,01,96");
            rowList.Add(Tram + ",1,24750170,59443400,@,01,96");
            rowList.Add(Tram + ",1,24750170,59443400,!,01,96");


            ParseGpsTxtFile result = new ParseGpsTxtFile(rowList);

            Assert.That(result.TransportationServiceList.Count, Is.EqualTo(1));
            Assert.True(result.TransportationServiceList.All(tp => tp.Type == TransportationType.Bus));
        }
        
        [Test]
        public void ParseSkipsNonSpecificFormat()
        {
            var rowList = new List<string>();
            rowList.Add(Bus + ",1,24750170,59443400,,10,96");
            rowList.Add(Tram + ",1,24750170,59443400,,10");
            
            ParseGpsTxtFile result = new ParseGpsTxtFile(rowList);

            Assert.That(result.TransportationServiceList.Count, Is.EqualTo(1));
            Assert.True(result.TransportationServiceList.All(tp => tp.Type == TransportationType.Bus));
        }
        
        [Test]
        public void ParseSkipsEmptyValueRow()
        {
            var rowList = new List<string>();
            rowList.Add(Bus + ",1,24750170,59443400,,10,96");
            rowList.Add(",1,24750170,59443400,,10,22");
            rowList.Add(Tram + ",,24750170,59443400,,10,22");
            rowList.Add(Tram + ",1,,59443400,,10,22");
            rowList.Add(Tram + ",1,24750170,,,10,22");
            rowList.Add(Tram + ",1,24750170,59443400,,,22");
            rowList.Add(Tram + ",1,24750170,59443400,,10,");
            
            ParseGpsTxtFile result = new ParseGpsTxtFile(rowList);

            Assert.That(result.TransportationServiceList.Count, Is.EqualTo(1));
            Assert.True(result.TransportationServiceList.All(tp => tp.Type == TransportationType.Bus));
        }
    }
}