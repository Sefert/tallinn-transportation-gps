﻿using NUnit.Framework;

namespace TallinnTransportGpsTest.UnitTest.DalGpsTxtTest
{
    public class ReadFileTest : BaseUnitTest
    {
        [Test]
        public void ReadFileReadsAllRows()
        {
            LogInfo("Collecting all txt file rows");

            Assert.That(CorrectFile.FileRowList.Count, Is.EqualTo(3));
            Assert.That(BrokenFile.FileRowList.Count, Is.EqualTo(2));
        }
    }
}