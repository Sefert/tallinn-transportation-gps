﻿using DAL;
using NUnit.Framework;

namespace TallinnTransportGpsTest.UnitTest.DalGpsTxtTest
{
    public class ParseGpsTxtFileTest : BaseUnitTest
    {
        [Test]
        public void ListHasSameAmountOfRowsAsReadFile()
        {
            ParseGpsTxtFile parseGpsTxtFile = new ParseGpsTxtFile(CorrectFile.FileRowList);

            Assert.AreEqual(CorrectFile.FileRowList.Count, parseGpsTxtFile.TransportationServiceList.Count);
        }
    }
}