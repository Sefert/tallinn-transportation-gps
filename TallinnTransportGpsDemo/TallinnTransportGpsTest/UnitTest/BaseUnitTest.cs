﻿using System;
using DAL;
using Domain.Enums;
using NUnit.Framework;

namespace TallinnTransportGpsTest.UnitTest
{
    public class BaseUnitTest : TestLog
    {
        protected int Trolleybus = (int)TransportationType.TrolleyBus; // value 1
        protected int Bus = (int)TransportationType.Bus; // value 2
        protected int Tram = (int)TransportationType.Tram; // value 3
        
        protected ReadFile CorrectFile, BrokenFile;
        
        protected void GetIgnoredRows(ParseGpsTxtFile result)
        {
            foreach (var elem in result.SkippedRow)
            {
                Console.WriteLine(elem);
            }
        }

        [OneTimeSetUp]
        protected override void OneTimeSetUp()
        {
            base.OneTimeSetUp();
            CorrectFile = new ReadFile("../../../TestData/gps.txt");
            BrokenFile = new ReadFile("../../../TestData/false.txt");
        }

        [OneTimeTearDown]
        protected virtual void TearDown()
        {
            
        }
    }
}