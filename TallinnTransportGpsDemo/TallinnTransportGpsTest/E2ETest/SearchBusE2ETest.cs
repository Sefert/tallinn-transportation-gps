﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using TallinnTransportGpsTest.Enum;
using TallinnTransportGpsTest.PageObject;
using TallinnTransportGpsTest.WebDriverFactory;

namespace TallinnTransportGpsTest.E2ETest
{
    [TestFixture(Browser.Chrome)]
    //[TestFixture(Browser.Edge)]
    [TestFixture(Browser.Firefox)]
    [Parallelizable(ParallelScope.Self)]
    public class SearchBusE2ETest : WebDriverSetup
    {
        public SearchBusE2ETest(Browser type) : base(type) {}

        [Test]
        public void SearchBusReturnsBusList()
        {
            Driver.Navigate().GoToUrl("https://localhost:5001");
            
            var mainPage = new TransportationMainPage(Driver);
            mainPage.BusLink.Click();
            
            var busPage = new TransportationBusPage(Driver);
            Driver.FindElement(By.ClassName("input-field")).Click();
            
            //new Actions(Driver).SendKeys(Keys.Shift+Keys.Control).Build().Perform();
            //System.Threading.Thread.Sleep(6000);
            /*IJavaScriptExecutor executor = (IJavaScriptExecutor)Driver;
            executor.ExecuteScript("arguments[0].setAttribute(arguments[1], arguments[2]);", 
                busPage.SearchBox, "value", "33");*/
            
            busPage.SearchBox.SendKeys("33");
            busPage.SearchButton.Click();
            
            WebDriverWait wait = new WebDriverWait(Driver,TimeSpan.FromSeconds(2));
            wait.Until(condition => condition.FindElement(By.CssSelector("textarea[value='33']")));
            
            /*string data = busPage.SearchBox.GetAttribute("value");
            Assert.True(Driver.FindElement(By.Id("transportation_number")).GetAttribute("value").Equals("33"));*/
            
            ReadOnlyCollection<IWebElement> busCollection = Driver.FindElements(By.CssSelector("div>a.btn"));

            Assert.True(busCollection.All(bus => bus.FindElement(
                By.CssSelector("div.row>div.col.s2")).Text.Equals("33")));
            
            busCollection.ElementAt(1).Click();

            //TODO: Assert redirected web page
            //Assert.True();
        }
    }
}