﻿using Domain.Settings.WebDriverSetting;
using Microsoft.Extensions.Configuration;


namespace TallinnTransportGpsTest
{
    public class TestConfiguration
    {
        //https://weblog.west-wind.com/posts/2018/Feb/18/Accessing-Configuration-in-NET-Core-Test-Projects
        //https://www.red-gate.com/simple-talk/dotnet/net-development/asp-net-core-3-0-configuration-factsheet/
        public static WebDriverConfiguration GetTestConfiguration(string outputPath)
        {
            var configuration = new WebDriverConfiguration();
            var iConfig = GetIConfigurationRoot(outputPath);
            iConfig.GetSection("WebDriverSetting").Bind(configuration);
            return configuration;
        }

        private static IConfigurationRoot GetIConfigurationRoot(string outputPath)
        {            
            return new ConfigurationBuilder()
            .SetBasePath(outputPath)
            .AddJsonFile("testsettings.json", optional: true)
            .AddEnvironmentVariables()
            .Build();
        }
    }
}