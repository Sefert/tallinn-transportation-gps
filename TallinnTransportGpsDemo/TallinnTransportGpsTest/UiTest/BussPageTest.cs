﻿using NUnit.Framework;
using TallinnTransportGpsTest.Enum;
using TallinnTransportGpsTest.PageObject;
using TallinnTransportGpsTest.WebDriverFactory;

namespace TallinnTransportGpsTest.UiTest
{
    [TestFixture(Browser.Chrome)]
    //[TestFixture(Browser.Edge)]
    [TestFixture(Browser.Firefox)]
    [Parallelizable(ParallelScope.Self)]
    public class BussPageTest : WebDriverSetup
    {
        public BussPageTest(Browser type) : base(type) {}

        [Test]
        public void BusPageElementsTextMatch()
        {
            Driver.Navigate().GoToUrl("https://localhost:5001/Tallinn/Bus");
            var busPage = new TransportationBusPage(Driver);

            Assert.AreEqual(busPage.SearchLabel.Text, "Transpordi liininumber");
            Assert.AreEqual(busPage.SearchButton.Text, "Otsi\r\nsend");
        }
    }
}