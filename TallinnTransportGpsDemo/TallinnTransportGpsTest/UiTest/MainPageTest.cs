﻿using NUnit.Framework;
using TallinnTransportGpsTest.Enum;
using TallinnTransportGpsTest.PageObject;
using TallinnTransportGpsTest.WebDriverFactory;

namespace TallinnTransportGpsTest.UiTest
{
    [TestFixture(Browser.Chrome)]
    //[TestFixture(Browser.Edge)]
    [TestFixture(Browser.Firefox)]
    [Parallelizable(ParallelScope.Self)]
    public class MainPageTest : WebDriverSetup
    {
        public MainPageTest(Browser type) : base(type) {}

        [Test]
        public void MainPageElementLinksTextMatch()
        {
            Driver.Navigate().GoToUrl("https://localhost:5001");
            var mainPage = new TransportationMainPage(Driver);
            
            Assert.AreEqual(mainPage.Privacy.Text, "Privacy");
            Assert.AreEqual(mainPage.BusLink.Text, "BUSS");
            Assert.AreEqual(mainPage.TramLink.Text, "TRAMM");
            Assert.AreEqual(mainPage.TrolleyBusLink.Text, "TROLL");
        }
    }
}