##### TallinnTransportSystemDemo  
Interpret https://transport.tallinn.ee/gps.txt  
Find and show transportation locations by case sensitive transportation service number.  
by Marko Linde
  
For API needed: [ NetCore 3.0 ](https://dotnet.microsoft.com/download)
  
Tests location: TallinnTransportGpsTest  
Basic factory, pageobjects for testing this API.

If integrated with some CI tool, the Selenium franework tests will fail(the API is not running).

The TDD tests should pass.
 

